class OnlineVid
 {
    private:  //properties
         unsigned  views;
         std::string id; 
         


    public:  //methods

         OnlineVid();
         OnlineVid(unsigned, std::string);
        
      
         void setViews(unsigned);
         unsigned getViews();

         void setId(std::string);
         std::string getId();


 };

